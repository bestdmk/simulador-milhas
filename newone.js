var App = App || {};
App.Components = App.Components || {};
App.Contas = App.Contas || {};
App.Simuladores= App.Simuladores || {};
var App = App || {};

App.Components.NavTabs = (function ($) {
    var o = {};
    
    o.currentTabActive = undefined;
    o.currentTabActiveIndex = 0;
    o.tabActiveIndex = 0; // TAB 1 ACTIVE

    o.initialize = function (options) {

        console.log("NavTabs :: initialize ");
        
        //===============================================================
        // NAV TABS - GET TAB ACTIVE
         // o.navTabsContainer = $(".bb-nav-tabs-container");
         o.tabActiveIndex = 0; // TAB 1 ACTIVE
         o.currentTabActive = $(".bb-nav-tabs .nav-item.active"); // TAB 1 ACTIVE

         o.navtabs = $(".bb-nav-tabs");
         o.navtabs.each(function(index, e){
           e.currentTabActiveIndex = 0; 
           e.currentTabActive = $(".nav-item.active", e)[0];
           // o.selectTab(e, o.tabActiveIndex);

         })

         setTimeout(o.checkNavTabsHeight, 250);

         // console.log("o.navtabs");
         // console.log(o.navtabs);
         // o.selectTab(o.tabActiveIndex);
        
        
        // NAV TABS TAB CLICK
        $(".bb-nav-tabs .nav-link").click(function(){ 

          curentNavtabs = $(this).closest(".bb-nav-tabs")[0]
          tabActiveIndex = $(this).parent().data("tabindex");
          o.selectTab(curentNavtabs, tabActiveIndex);  
        })

        // NAV TAB - MOBILE COLLAPSED HEADER CLICK 
         var tabCollapseActiveIndex  = -1;
         $(".bb-jsNavTabTrigger").click(function(){ 
            setTimeout(o.checkNavTabsHeight, 350);
         })

        return o;
    };

    o.selectTab = function(curentNavtabs,  tabIndex) {
        
        console.log("CURRENT selectTab");
        console.log(curentNavtabs.currentTabActive);

        var contentID = undefined;

        if(curentNavtabs.currentTabActive != undefined){
          // console.log("curentNavtabs.currentTabActive");
          contentID = $("a", curentNavtabs.currentTabActive).attr("href");
          contentDom = $(contentID);


          $(curentNavtabs.currentTabActive).removeClass("tri-down");
          $(curentNavtabs.currentTabActive).removeClass("opened");
          $('.nav-link', curentNavtabs.currentTabActive).removeClass("active")
          // $('.nav-link', o.currentTabActive).removeClass("active")
         
          if(contentDom){
            if(contentDom.hasClass("active")){
                contentDom.removeClass("active").removeClass("show");
                // contentDom.addClass("active");
            }
          }
        }
        
        curentNavtabs.currentTabActive =   $('li.nav-item[data-tabindex="'+tabIndex+'"]', curentNavtabs); 
        console.log("curentNavtabs.currentTabActive");
        console.log($("a", curentNavtabs.currentTabActive))
        contentID = $("a", curentNavtabs.currentTabActive).attr("href");
        contentDom = $(contentID);






        if(!curentNavtabs.currentTabActive.hasClass("opened")){
          console.log("selectTab ####");
          curentNavtabs.currentTabActive.addClass("tri-down");
          curentNavtabs.currentTabActive.addClass("opened");
          if(contentDom){
              if(!contentDom.hasClass("active")){
                  contentDom.addClass("active").addClass("show");
                  // contentDom.addClass("active");
              }
            }
          } 

          setTimeout(o.checkNavTabsHeight, 350);
         
    }

    o.checkNavTabsHeight = function () { 

        $(".bb-nav-tabs-container").each(function(index, e){

          var h = $(".tab-content", $(this)).height() + 350;
          console.log("checkNavTabsHeight: ")
          // console.log(e)
          console.log("checkNavTabsHeight: " + h)
          $(this).css({"paddingBottom": h});

        })

        
    }

    o.resize = function() {

    }

    return o;
    
}(jQuery));

App.Contas.Home = (function ($) {§
    var o = {};
    var currentItemOpened = undefined;
    var rangeSlider = undefined;

    o.initialize = function (options) {



        if ($("a[href='#anchorSimuladorTAPVitoria']").length > 0) {

            $("a[href='#anchorSimuladorTAPVitoria']").click(function (e) {


                $('html, body').animate({ scrollTop: 2000 }, 1000);
            });
        }

        $("#tabs-2-tab-A").on("shown.bs.tab",function(){
            $("#tabs-2-pane-A .novo_cliente_toggle[data-value='1']").trigger("click")
            $("#tabs-2-pane-A .envolvimento_toggle[data-value='1']").trigger("click")
        });

        return o;
    };




    o.resize = function () {

        // $('#searchModal').data('bs.modal').handleUpdate()
        //adjust Height of #sidebar-mainContainer
        // mainContainer.css({"min-height": $(window).height()});
    }

    return o;

}(jQuery));

App.Simuladores.Base = (function ($) {
    var o = {};

    o.initialize = function (options) {

       //console.log("App.Simulador.Base :: init");

        o.rangeSlider = App.Components.RangeSlider.initialize();


        // FIND AND SETUP INPUT CLEAR BTN X
        function tog(v){return v?'addClass':'removeClass';} 
          $(document).on('input', '.clearable', function(){
              $(this)[tog(this.value)]('x');
          }).on('mousemove', '.x', function( e ){
              $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
          }).on('touchstart click', '.onX', function( ev ){
              ev.preventDefault();
              $(this).removeClass('x onX').val('').change();
          });

        return o; 

    };


    o.toogleCollapseSubitems = function(colindexClicked, rowindexClicked) {

      $(".bb-comparador__item__subitem-expand-content").each(function(index, el){
          
          var colindex = $(this).data("colindex");
          var rowindex = $(this).data("rowindex");

          // console.log(colindex);
          // console.log(rowindex);
          // console.log("############");
         
          if(rowindex == rowindexClicked){

            // console.log("#333333333. 333 3############");
            $(this).toggleClass("--opened");
          }else{
            $(this).removeClass("--opened");    

            
          }


          // if($(this).)

      });

      
    }
    


    return o;
    
}(jQuery));


if (document.all) {
      App.Components.NavTabs.initialize();
      App.Contas.Home.initialize();
      App.Simuladores.Base.initialize();
} else {
    $(function () {
      App.Components.NavTabs.initialize();
      App.Contas.Home.initialize();
      App.Simuladores.Base.initialize();
    });
}



(function($, window, document, undefined) {
	var pluginName = 'simuladorTAPVitoria',
		$this = null,
		self = this;

		
	var SimuladorTAPVitoria = function( element, options ) {
        this.element = $(element);
        this._name = pluginName + parseInt(Math.random() * 999999999);
        // this._name = pluginName;


		$this = this;
		
		this.elem = {};
		
        this.init(options);	
    }
	
	
	// Init plugin
	SimuladorTAPVitoria.prototype = {
        
		init: function(options) {
			
			this.options = options;
			
			this.elem.valor = $("#ceSliderValor", this.element);
			this.elem.cartao = $("input[name='cartao']", this.element);
			this.elem.cliente = $("input[name='cliente']", this.element);
			this.elem.envolvimento = $("input[name='envolvimento']", this.element);
			
			// Slider input
			var valorvalue = this.elem.valor.next().next();
			this.elem.valorValue = $("input", valorvalue);

			this.elem.limpar = $(".passo1 .btn-limpar", this.element);
			this.elem.calcular = $(".passo1 .btn-calcular", this.element);
			
			this.elem.quadro = $("#tvQuadro", this.element);
			this.elem.tabela = $("#tvTabela", this.element);
						
			this.elem.lockEnvolvimento = $(".parametro.sim-envolvimento a.cadeado", this.element);
			this.elem.lockValor = $(".parametro.sim-valor a.cadeado", this.element);

			//console.log("SimuladorTAPVitoria init");
			
			
			// Setup Sliders
			this.elem.valor.attr({
                    min: parseFloat(this.options.valorMin),
					max: parseFloat(this.options.valorMax),
					value: parseFloat(this.options.valor),
					step: this.options.valorStep,
                    labels: this.options.valorMin + "," + options.valorMax
                }
             );
            this.elem.valor.rangeslider('update', true);
            this.elem.valor.on('change', this.sliderChanged);
            this.elem.valor.trigger("slider");

			// this.elem.valor.slider({
			// 	min: parseFloat(this.options.valorMin),
			// 	max: parseFloat(this.options.valorMax),
			// 	value: parseFloat(this.options.valor),
			// 	step: this.options.valorStep
			// })
			// .bind('slide', $.proxy(this.sliderChanged, this))
			// .bind('slideStart', $.proxy(this.checkLocked, this))
			// .trigger('slide');
			
			this.elem.lockValor.bind("click", $.proxy(this.switchLock, this));
			
			this.elem.valorValue.val(this.options.valor);
			
			// Allow only numbers
			this.elem.valorValue.bind("keydown", $.proxy(this.blockLetters, this));
			
			// Field updates
			this.elem.valorValue.bind("change", $.proxy(this.sliderValueUpdate, this));
			
			this.elem.valorValue.bind("blur", $.proxy(this.sliderValueUpdate, this));
			
			this.elem.limpar.bind("click", $.proxy(this.limpar, this));
			this.elem.calcular.bind("click", $.proxy(this.validar, this));

			// $("a.cadeado", this.element).tooltip();
			
			// Login Button
			// $("p.fazerlogin a", this.element).click(function() {	
			// 	try{
			// 		$(".content_login .button.button-login-top").trigger("click");
			// 	}catch(e){};
			// });
			
			// Clear button
			$("input + a.best-input-icon", this.element).click(function() {
				$(this).prev().val("0");
			});
			
			// É CLIENTE?
			this.elem.cliente.val($(".novo_cliente_toggle").data("value"));
			this.elem.cliente.data("points", $(".novo_cliente_toggle").data("points"))
			this.elem.cliente.trigger("change");

			$(".novo_cliente_toggle").click($.proxy(function(e) {
				
				this.elem.cliente.val($(e.currentTarget).data("value"));
				this.elem.cliente.data("points", $(e.currentTarget).data("points"))
			    this.elem.cliente.trigger("change");

			    // console.log(this.elem.cliente.val())

                if( this.elem.cliente.val() == 0 ) {
					$(".bb-jsEnvolvimento-container").addClass("d-none");
                }
                else {
					$(".bb-jsEnvolvimento-container").removeClass("d-none");
                }
					this.calculate();
				}, this));
			

			// PATRIM > 25000
			this.elem.envolvimento.val($(".envolvimento_toggle").data("value"));
			this.elem.envolvimento.data("points",$(".envolvimento_toggle").data("points"));
			this.elem.envolvimento.trigger("change");

			$(".envolvimento_toggle").click($.proxy(function(e) {
				this.elem.envolvimento.val($(e.currentTarget).data("value"));
				this.elem.envolvimento.data("points", $(e.currentTarget).data("points"))
			    this.elem.envolvimento.trigger("change");
				this.calculate();
			}, this));
			
			// $(".novo_cliente_toggle.active").on("click",function(e){
			// 	e.preventDefault();
			// }).trigger("click");

			// $(".envolvimento_toggle.active").on("click",function(e){
			// 	e.preventDefault();
			// }).trigger("click");


			// this.elem.envolvimento.next().find("button").click($.proxy(function(e) {
			// 	var input = $(e.currentTarget).closest(".field").find("input");
			// 	input.val($(e.currentTarget).data("value")).data("points", $(e.currentTarget).data("points")).trigger("change");
			// 	this.calculate();
			// }, this));
			
			// this.elem.envolvimento.next().find("button.active").trigger("click");
			
			
			this.elem.cliente.change($.proxy(this.calculate, this));
			this.elem.envolvimento.change($.proxy(this.calculate, this));
			this.elem.cartao.change($.proxy(this.calculate, this));
			this.elem.valorValue.change($.proxy(this.calculate, this)).trigger("change");
			this.elem.valorValue.keyup($.proxy(this.calculate, this));
			
			
			
			
			this.limpar(null);
			
			this.updateLocks();

			this.calculate();

			var selected = undefined;
			$(".cartao-dropdown-menu").change(function(e) {
             
                selected = $(e.target.selectedOptions)[0];

                if($(selected).attr("id") == "nao-aderir"){
                	$('.bb-jsValormensal-container').fadeOut();
                	// $(".cartao-dropdown-menu").css("width" , "300px")
                } else {
                	 $('.bb-jsValormensal-container').fadeIn();
                }

                $this.elem.cartao.val($(selected).data("value"));
				$this.elem.cartao.data("points", $(selected).data("points"))
			    $this.elem.cartao.trigger("change");

            });
			
		},
		
		
		
		blockLetters: function(event) {
			
			
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				 // Allow: Ctrl+A
				(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: home, end, left, right
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			else {
				// Ensure that it is a number and stop the keypress
				if (event.keyCode == "190" && $(event.currentTarget).val().length > 0) {
				
				} 
				else if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		},
		
		limpar: function(e) {
			if (e) e.preventDefault();
			this.elem.valorValue.val(this.options.valor).trigger("change").trigger("blur");	
			// ContasCartoes.All.setValue(this.element, "cartao", this.options.cartao);
			// ContasCartoes.All.setValue(this.element, "cliente", this.options.cliente);
			// ContasCartoes.All.setValue(this.element, "envolvimento", this.options.envolvimento);
			this.calculate();
		},
		
		validar: function(e) {
			if (e) e.preventDefault();
			var valid = true;
			valid = ContasCartoes.All.validarBulk(this.element) && valid;
			
			if (valid) {
				$(".passo-b", this.element).show();
			} else {
				
			}
		},
		
		sliderValueUpdate: function(e) {

			this.sliderValueChanged(e, true);

			// s.attr({ value: field.val()});
   //         		s.rangeslider('update', true);

   //         		this.calculate.apply(this);


			// var which = e.currentTarget;


			// this.vars.cp = parseFloat(this.elem.cp.val());
			// this.vars.profit =  parseFloat(this.elem.profit.val());
			// this.vars.loss =  parseFloat(this.elem.loss.val());
			
			// this.sliderValueChanged(e, true);
		},
		
		sliderValueChanged: function(e, checkmin) {
			if (typeof(checkmin) == "undefined") checkmin = false;
			
			var field = $(e.currentTarget);
			var s = $(e.currentTarget).parent().prev().prev();


			//console.log("### sliderValueUpdate")
			//console.log("### sliderValueUpdate")

			var minValue = s.attr("min");
			var maxValue = s.attr("max");
			
			var val = parseFloat(field.val());
			
			if (checkmin) {
				if (val > parseFloat(maxValue)) field.val(maxValue);
				if (val < parseFloat(minValue)) field.val(minValue);
			}
			
			// s.data('slider').setValue(field.val());
			s.attr({ value: field.val()});
           	s.rangeslider('update', true);

			
			//console.log("### sliderValueUpdate")

			this.calculate.apply(this);
		},
		
		sliderChanged:  function(e) {
			
			// var which = e.currentTarget;

			// var which = this.id;

			// var eValue = parseFloat(e.target.value);


			
			// var valfield = $(which).parent().parent().parent().find(".sim-slider-value");
			// console.log(this.elem);

			// var val = e.value;
			
			// if (val % parseInt(val) != 0 && val) val = val.toFixed(2);
			// this.elem.valorValue.val(val);

			var target = e.currentTarget;
			var val = target.value;
			var valfield = $(target).parent().find(".rangeslider__handle__value input");
			
			if (val != 0 && val){

				if (val % parseInt(val) != 0){
					val = val.toFixed(2);
				} else {
					valfield.val(val);
				}
				
			} else {
				valfield.val(0);
			}
			
			

			// var tss = $("#tabSimuladorTAPVitoria").simuladorTAPVitoria;

			//console.log("val");
			//console.log(val);
			//console.log("valfield");
			//console.log(valfield);

			$this.calculate.apply($this);
		},
		
		
		switchLock: function(e) {
			$(e.currentTarget).toggleClass("locked");
			this.updateLocks();
		},
		
		checkLocked: function(e) {
			var isLocked = $(e.currentTarget).hasClass("locked");
			
			if ($(e.currentTarget).hasClass("locked")) {
				e.preventDefault();	
			}
			return isLocked;
		},
		
		
		updateLocks: function() {
			
			// if (this.elem.lockValor.hasClass("locked"))  {
			// 	this.elem.valor.addClass("locked");
			// 	this.elem.valor.parent().parent().parent().addClass("locked");
			// 	this.elem.valorValue.attr("disabled", "disabled");
			// 	this.elem.valor.slider("setLocked", true);
			// } else {
			// 	this.elem.valor.removeClass("locked");
			// 	this.elem.valor.parent().parent().parent().removeClass("locked");
			// 	this.elem.valorValue.removeAttr("disabled");
			// 	this.elem.valor.slider("setLocked", false);
			// }
		},
		
		
		calculate: function() {

			
			var cliente = parseInt(this.elem.cliente.val());
			var cartao = parseInt(this.elem.cartao.val());
			var envolvimento = parseInt(this.elem.envolvimento.val());
			var valor = parseInt(this.elem.valorValue.val());
			
			
			if (valor < this.options.valorMin || this.elem.valorValue.val() == "") {
				valor = this.options.valorMin;
				this.elem.valorValue.val(valor).trigger("change").trigger("blur");
			}
			
			//variaveis intermedias
			var _valueCC = 0;
			var _valuetdCC = 0;
			var _valuetdEnvolvimento = 0;
			var _valuetdNovo = 0;
			
			var _valorComprasMesCC = 0;
			var _valorComprasCC1Ano = 0;
			var _valorComprasCC3Ano = 0;
			
			var _totalFundosInvestimento = 0;
			var _totalProdutosObrigacoes = 0;
			var _totalSegurosCapitalizacao = 0;
			var _totalPlanosPoupanca = 0;
			var _totalProdutosPoupancaMensal = 0;
			var _totalProdutosPoupancao1Ano = 0;
			var _totalProdutosPoupanca3Ano = 0;
			
			var _numberBolsa = 0;
			var _numberBolsa1Ano = 0;
			var _numberBolsa3Ano = 0;
			
			var _totalMilhasMes = 0;
			var _totalMilhas1Ano = 0;
			var _totalMilhhas3Ano = 0;
			
			
			// Account Type
			_valueCC = this.options.AccountType * 1;
			
			// Credit Card
			_valuetdCC = this.elem.cartao.parent().find(".cartao-dropdown-menu option[data-value='" + this.elem.cartao.val() + "']").data("points") * 1;

			//console.log("##### CALCULATE");
			//console.log(_valuetdCC);
			//console.log(this.elem.cartao);
			//console.log(this.elem.cartao.parent().find(".cartao-dropdown-menu option[data-value='" + this.elem.cartao.val() + "']"))
			
			_valorSlotMilhas = 100;
			_valorSlotEuros = 100;

			// New client
			_valuetdNovo = this.elem.cliente.data("points") * 1;

			// console.log(this.elem.cliente);
			
			
			// Envolvimento
			_valuetdEnvolvimento = envolvimento == true && cliente != 0 ? 10000 : 0;
			
			// Valor mensal de compras
			try{
				_valcomprasCC = this.elem.valorValue.val().replace(',', '.') * 1;			
			}catch(e){
				_valcomprasCC = 0;
			}
			_valorComprasMesCC = Math.floor(_valcomprasCC / this.options.CCEuros) * this.options.CCMilhas;
			_valorMilhasMesCC = parseInt((_valorComprasMesCC/_valorSlotEuros))*_valorSlotMilhas;
			_valorMilhasTrimesCC = _valorMilhasMesCC * 3;
			_valorMilhasCC1Ano = _valorMilhasMesCC * 12;
			_valorMilhasCC3Ano = _valorMilhasCC1Ano * 3;
		    //_valorComprasCC1Ano = _valorComprasMesCC * 12;
			//_valorComprasCC3Ano = _valorComprasCC1Ano*3;
			
			
			
			// Total de Milhas
			_totalMilhasTrimes = _valueCC + _valuetdCC + _valuetdEnvolvimento + _valuetdNovo + _valorMilhasTrimesCC + _totalProdutosPoupancaMensal + _numberBolsa;
		    _totalMilhas1Ano = _valueCC + _valuetdCC + _valuetdEnvolvimento + _valuetdNovo + _valorMilhasCC1Ano + _totalProdutosPoupancao1Ano + _numberBolsa1Ano;
		    _totalMilhhas3Ano = _valueCC + _valuetdCC + _valuetdEnvolvimento + _valuetdNovo + _valorMilhasCC3Ano + _totalProdutosPoupanca3Ano + _numberBolsa3Ano;
		    
			
			// Mostrar Resultados
			//console.log(parseInt((_valorComprasMesCC/_valorSlotEuros))*_valorSlotMilhas);
			//_valorMensalCartao = parseInt((_valorComprasMesCC/_valorSlotEuros))*_valorSlotMilhas;

			// console.log($("#tvtabela1", this.element));
			// console.log(_valuetdNovo);

		    $(".val1a", this.element).text(_totalMilhasTrimes.formatMoney(0, ',', '.'));
		    $("#val1b", this.element).text(_totalMilhasTrimes.formatMoney(0, ',', '.'));
		    $(".val2b", this.element).text(_totalMilhas1Ano.formatMoney(0, ',', '.'));
		    $(".val3b", this.element).text(_totalMilhhas3Ano.formatMoney(0, ',', '.'));
		    
		    $("#tvtabela1", this.element).text(_valuetdNovo.formatMoney(0, ',', '.'));
		    $("#tvtabela2", this.element).text(_valuetdEnvolvimento.formatMoney(0, ',', '.'));
		    $("#tvtabela3", this.element).text(_valuetdCC.formatMoney(0, ',', '.'));
		    $("#tvtabela4", this.element).text(_valorComprasMesCC.formatMoney(0, ',', '.'));
		    
		}
		
    };
	
	
	
	
	$.fn[pluginName] = function ( option, val ) {
		return this.each(function () {
			var $this = $(this),
				data = $this.data(pluginName),
				options = typeof option === 'object' && option;
			if (!data)  {
				$this.data(pluginName, (data = new SimuladorTAPVitoria(this, $.extend({}, $.fn[pluginName].defaults, options))));
			}
			if (typeof option == 'string') {
				data[option](val);
			}
		})
	};

	
	
	$.fn[pluginName].defaults = {
		cliente: 1,
		cartao: "amex",
		envolvimento: 1,
		valor: 100,
		valorMin: 0,
		valorMax: 10000,
		valorStep: 100,

		
		// Constantes
		SubsProdutosMilhas: 100, // G2
		SubsProdutosEuros: 10000, //J2
		SubsProdutosCap: 12000, //M2
		
		OrdensBolsaMilhas: 5, //G3
		OrdensBolsaEuros: 10000,//J3
		
		CCMilhas: 1, // G4
		CCEuros: 1, //J4 
		
		AccountType: 0,
		ordcompras: 0
		
	};

	$.fn[pluginName].Constructor = SimuladorTAPVitoria;
	
})(jQuery, window, document);







