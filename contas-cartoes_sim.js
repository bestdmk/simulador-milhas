Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
 
var IR2 = jQuery.extend({}, typeof(IR2) == "object" ? IR2 : {});

(function($) {

	ContasCartoes.Simuladores = $.extend(ContasCartoes.Simuladores || {}, {
		
		
		init: function() {
			var $this = this;
			
			// if (jQuery().comparadorCartoes) $("#tabComparadorCartoes").comparadorCartoes({});
			// if (jQuery().simuladorComprasEspeciais) $("#tabSimuladorComprasEspeciais").simuladorComprasEspeciais({});
			// if (jQuery().simuladorPremiacao) $("#tabSimuladorPremiacao").simuladorPremiacao({});
			// if (jQuery().simuladorTAPVitoria) $("#tabSimuladorTAPVitoria").simuladorTAPVitoria({});
			
		}
		
		
			
	});
	
	$(document).ready(function() {
		ContasCartoes.Simuladores.init();
	});

})(jQuery);