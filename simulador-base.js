var App = App || {};
App.Simuladores= App.Simuladores || {};

App.Simuladores.Base = (function ($) {
    var o = {};

    o.initialize = function (options) {

       //console.log("App.Simulador.Base :: init");

        o.rangeSlider = App.Components.RangeSlider.initialize();


        // FIND AND SETUP INPUT CLEAR BTN X
        function tog(v){return v?'addClass':'removeClass';} 
          $(document).on('input', '.clearable', function(){
              $(this)[tog(this.value)]('x');
          }).on('mousemove', '.x', function( e ){
              $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
          }).on('touchstart click', '.onX', function( ev ){
              ev.preventDefault();
              $(this).removeClass('x onX').val('').change();
          });

        return o; 

    };


    o.toogleCollapseSubitems = function(colindexClicked, rowindexClicked) {

      $(".bb-comparador__item__subitem-expand-content").each(function(index, el){
          
          var colindex = $(this).data("colindex");
          var rowindex = $(this).data("rowindex");

          // console.log(colindex);
          // console.log(rowindex);
          // console.log("############");
         
          if(rowindex == rowindexClicked){

            // console.log("#333333333. 333 3############");
            $(this).toggleClass("--opened");
          }else{
            $(this).removeClass("--opened");    

            
          }


          // if($(this).)

      });

      
    }
    


    return o;
    
}(jQuery));


if (document.all) {
    App.Simuladores.Base.initialize();
} else {
    $(function () {
       App.Simuladores.Base.initialize();
    });
}