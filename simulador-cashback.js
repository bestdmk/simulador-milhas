var App = App || {};
App.Tabelas = App.Tabelas || {};

App.Tabelas.Detalhes = (function($) {
    var o = {};
    var currentSlideActive = undefined;
    var currentSlideIndex = 0;
    var boxesAlreadyOpened = false;

    var currentTabActive = undefined;
    var currentTabActiveIndex = 0;

    o.initialize = function(options) {
      
        $(".rangeslider").css("width", "100%");
        $("#tabSimuladorPremiacao").simuladorPremiacao({});
        $(".rangeslider__handle__value", $("#ceSliderValor_cashback").parent()).css("opacity", 0);
        

        return o;
    };


    o.resize = function() {

    }

    return o;

}(jQuery));

if (document.all) {
    App.Tabelas.Detalhes .initialize();
} else {
    $(function() {
        App.Tabelas.Detalhes.initialize();
    });
}