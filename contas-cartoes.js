$.fn.outside = function(ename, cb){
    return this.each(function(){
        var $this = $(this),
            self = this;

        $(document).bind(ename, function tempo(e){
            if(e.target !== self && !$.contains(self, e.target)){
                cb.apply(self, [e]);
                if(!self.parentNode) $(document.body).unbind(ename, tempo);
            }
        });
    });
};

var ContasCartoes = jQuery.extend({}, typeof(ContasCartoes) == "object" ? ContasCartoes : {});

(function($) {

	ContasCartoes.All = $.extend(ContasCartoes.All || {}, {
		
		container: null,
		
		init: function() {
			
			this.container = $("#contascartoes_main");

			
			var $this = this;
			
			// Error tooltip
			// $tooltip = $('<div class="error-tooltip">Campo inválido.</div>').prependTo("body");
			
			// this.container.on("mouseover", ".best-input, .bestdrop, .best-drop, .checkradio span, .dropdown-toggle, textarea, .btn-group", function() {
			// 	if ($(this).hasClass("error")) {
			// 		var offset = $(this).offset();
			// 		$(".error-tooltip").css({left:offset.left-28, top:offset.top -45}).show();
			// 	}
			// }).on("mouseout", function() {
			// 	$(".error-tooltip").hide();
			// });
			
			
			this.container.on("change", "input, textarea", function() {
				if (typeof(ContasCartoes.All) != "undefined") ContasCartoes.All.saved = false;
				$(this).removeClass("error");
				$(this).parent().removeClass("error");
				$(this).parent().parent().removeClass("error");
				if ($(this).attr("type") == "radio" || $(this).attr("type") == "checkbox") {
					$(this).parents(".checkradio").removeClass("error").removeClass("error");
					if ($(this).closest(".checkgroup").length > 0) {
						$(".checkradio", $(this).closest(".checkgroup")).each(function() {
							$(this).removeClass("error");
						})
					}
				}
			});
			
			this.container.on("keyup", "input, textarea", function() {
	        	$(this).removeClass("error");
	        });
	        
	        this.container.on("click", ".dropdown-toggle", function(e){
	        	$(this).removeClass("error");
	        	if ($(this).parent().hasClass("disabled")) {
	        		e.preventDefault();
	        		return;
	        	}
	        });
	        
	        
			
			$("input + a.best-input-icon").click(function() {
				$(this).prev().val("");
			});
			
			$(".btn-group button").click(function() {
				$(this).parent().removeClass("error");
			});
			
			
			$("body").on("keydown", "input[data-restrict='NAME']", function(e) {
				var a = "ÇçüéâäàåçêëèïîìÄÅÉôöòûùÿÖÜáíóúñÑÁÂÀãÃÊËÈÍÎÏÓÔÒõÕÚÛÙýÝ'. ";
					var k;
				    document.all ? k = e.keyCode : k = e.which;
				    if (!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 37 || k == 39 || a.indexOf(String.fromCharCode(k)) != -1 || k == 186 || k == 222 || k == 190 || k == 11 || k == 9)) e.preventDefault();
			});
			
			$("body").on("keydown", "input[data-restrict='DECIMAL']", function(e) {
				var k;
				    document.all ? k = e.keyCode : k = e.which;
				    if (!((k >= 48 && k <= 57) || (k >= 96 && k <= 105) || k == 110 || k == 8 || k == 32 || k == 190 || k == 188 || k == 11 || k == 9 || k == 37 || k == 39)) e.preventDefault();
			});
			
			$("body").on("keydown", "input[data-restrict='INT'], input[data-restrict='PHONE']", function(e) {
				var k;
				    document.all ? k = e.keyCode : k = e.which;
				    if (!((k >= 48 && k <= 57) || (k >= 96 && k <= 105) || k == 8 || k == 32 || k == 11 || k == 9 || k == 37 || k == 39)) e.preventDefault();
			});
			
			// INIT DATE PICKER
			if (jQuery().datepicker) {
				$('.best-input.date', this.container).datepicker({ changeMonth: true, changeYear: true, dateFormat: "dd/mm/yy", monthNames: [ "Janueiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ], dayNames: [ "Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado" ], dayNamesMin: [ "D", "S", "T", "Q", "Q", "S", "S" ] }).attr("readonly", true);
				
				$('.input_date > span').each(function() {
					$(this).click(function() {
						$(this).parent().find(".best-input.date").datepicker("show");
					});
				});
			}
			
			
			// PROCESS ALL DROPS
			$(".bestdrop .dropdown-menu a, .bestdrop .dropdown-single a", this.container).off("click");
			$(this.container).on("click", ".bestdrop .dropdown-menu a, .bestdrop .dropdown-single a", function(e) {
				
				
				if ($(this).closest(".bestdrop").hasClass("drop-suggested")) return;
				
				if ($(this).data("toggle") == "tab") { // Fix tabs on dropdowns
					$(this).tab('show');
					var drop = $(this).closest(".bestdrop");
					drop.find("li").not($(this).closest("li")).removeClass("active");
				}
				var text = $(this).find("h4").length > 0 ? $(this).find("h4").html() : $(this).html();
				$(this).parents(".bestdrop").find(".dropdown-toggle").html(text + '<span class="dropseta"></span>');
				$(this).parents(".bestdrop").find("input").val($(this).data("value")).trigger("change");
				if (!$(this).closest(".bestdrop").hasClass("tabledrop") || $(this).attr("href").substr(0, 1) == "#") {
					e.preventDefault();
				} else {
					window.location.href = $(this).attr("href");
				}
				
				$(this).closest(".bestdrop").trigger("changed");
			});

			
			
			
			// Drop suggested items
			$(".bestdrop.drop-suggested .dropdown-menu a").unbind("click").click(function(e) {
				var text = $(this).text();
				$(this).parents(".drop-suggested").find("input").val(text).trigger("change");
				e.preventDefault();
			});

			
			$(".not_container .close").unbind("click").click(function(e) {
				
				e.preventDefault();
				
				var block = $(e.currentTarget).closest(".not_container");
				var main = $(".not_main", block);
			
				if (typeof($this.timer) != "undefined") clearTimeout($this.timer);
				block.slideToggle(function() {
					$("> div", block).hide();
					main.show();
					block.removeClass("sending");
				});
			});
			
			
			
			// Footer Paging
			$(".footer-paging a.carregar-mais").unbind("click").click(function(e) {
				
				e.preventDefault();
				
				var $thisthis = this;
				var table = $(this).parents("table");
				var lastOption = $("tbody tr:last-child", table);
				var pager = $("p", $(this).parent());
				var page = parseInt(pager.data("page"));
				var pagesize = parseInt(pager.data("pagesize"));
				
				$(this).addClass("loading");
				
				// last 10 rows
				$last10 = $('> tbody > tr:not(.tablesorter-childRow)', table).slice(-10);
				
				setTimeout(function() {
					
					$last10.each(function() {
						$("tbody", table).append("<tr class='loaded' data-template='" + $(this).data("template") + "'>" + $(this).html() + "</tr>");
					});
					
					page++;
					pager.data("page", page);
					
					$("span", pager).html((page + 1) * pagesize);
					
					$($thisthis).removeClass("loading");
					
					$("body, html").animate({scrollTop: lastOption.offset().top + 35});
					
					table.trigger("update");
					
					if ($(".footer-paging .reset", table).length > 0) $(".footer-paging .reset", table).fadeIn();
					try{
						top.updateIframe( $("body").height() );	
					}catch(e){}
				}, 500);
				
				
				
			});	// END OF $(".footer-paging a.carregar-mais")
			
			
			
			$(".footer-paging .reset").unbind("click").click(function(e) {
				var table = $(this).closest("table");
				var pagesize = $(".footer-paging p").data("pagesize");
				var $this = this;
				
				$("html, body").animate({scrollTop: table.offset().top - 120}, {duration: 400, queue: false, complete: function() {
					$("tbody tr.loaded", table).fadeOut(250, function() {
						$(this).remove();
					});
					$(".footer-paging p").data("page", 0);
					$(".footer-paging p span", table).text(pagesize);
					$($this).hide();
					top.updateIframe( $("body").height() );	
				}});
				
				e.preventDefault();
				
			});
			
			
			
			$(".popup").on("click", "a.close", function(e) {
				$(this).parents(".popup").hide();
				e.preventDefault();
			});
			
			$("body").on("click", "*[data-toggle='popup']", function(e) {
				e.preventDefault();
				var to_open = $(this).parent().find(".popup");
				var xx = $(this).offset().left / $("body").width();

				if (xx < 0.5) {
					to_open.addClass("popup-left");
					to_open.removeClass("popup-right");
				} else {
					to_open.addClass("popup-right");
					to_open.removeClass("popup-left");
				}
				$(".popup").not(to_open).hide();
				to_open.fadeToggle();
			});
			
			
			$(".best-table-container").on("click", "a[data-remove*='remover']", function(e) {
				var popup = $($(this).data("remove"));
				$(".step", popup).hide();
				$(".step:eq(0)", popup).show();
				$(this).css("position", "relative");
				popup.show();
				var x = $(this).offset().left - popup.width() + 37;
				var y = $(this).offset().top - popup.height() - 12;
				popup.css({top: y, left: x, position: "absolute"});
				$("body").prepend(popup);
				e.preventDefault();
			});
			
			
		},
		
		
		
		
		validFormat: function(field) {
			
			var val, min, max, restrict;
			
			val = field.val();
			if (val == "") return true;
			
			var min = field.data("min");
			var max = field.data("max");
			var restrict = field.data("restrict");
			
			if (typeof(min) == "undefined") min = 0;
			if (typeof(max) == "undefined") max = null;
			if (typeof(restrict) == "undefined") restrict = null;			


			if (restrict == "DECIMAL" || restrict == "DECIMALVAR") {
				val = val.replace(",",".");
			}
			
			if (min) {
				if (parseFloat(val) < parseFloat(min)) return false;
			}
			
			if (max) {
				if (parseFloat(val) > parseFloat(max)) return false;
			}
			
			if (restrict == "INT") {
				return (val.match(/^[0-9]+$/));
			} else if (restrict == "PHONE") {
				return (val.match(/^[0-9]+$/) && val.length >= 9);
			} else if (restrict == "DECIMAL") {
				return (val.match(/^[0-9]+([,.][0-9]+)?$/));
			} else if (restrict == "DECIMALVAR") {
				return (val.match(/^[-+]?[0-9]+([,.][0-9]+)?$/));
			} else if (restrict == "INTVAR") {
				return (val.match(/^[-+]?[0-9]+$/));
			} else if (restrict == "EMAIL") {
				return (val.match(/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/));
			}
			
			return true;
			
		},
		
		validCurrency: function(val, min, max, integer, variation) {
			if (val == "") return true;
			if (typeof(min) == "undefined") min = 0;
			if (typeof(max) == "undefined") max = null;
			if (typeof(integer) == "undefined") integer = false;
			if (typeof(variation) == "undefined") variation = false;
			
			val = val.replace(",",".");
			
			if (min) {
				if (parseFloat(val) < parseFloat(min)) return false;
			}
			
			if (max) {
				if (parseFloat(val) > parseFloat(max)) return false;
			}
			
			if (integer) {
				return variation ? (val.match(/^[-+]?[0-9]+$/)) : (val.match(/^[0-9]+$/));
			} else {
				return variation ? (val.match(/^[-+]?[0-9]+([,.][0-9]+)?$/)) : (val.match(/^[0-9]+([,.][0-9]+)?$/));
			}
		},
		
		validVariation: function(val) {
			var reg = new RegExp(/^[-+]?([0-9]*[,.][0-9]*)|([0-9]+)$/);
			return (val.match(reg));
		},
		
		
		
		resetField: function(field) {
			
			if (field.length == 0) {
				return;
			}
			if (field.attr("disabled") == "disabled") return;
			
			var def = field.data("default");
			
			if (field.parent().hasClass("bestdrop")) {
				var drop = field.parent();
				var arrow = $(".dropdown-toggle span", drop);
				if (arrow) arrow = arrow; else arrow = null;
				if (def != null && typeof(def) != "undefined") {
					var toggle = $(".dropdown-toggle", drop);
					var li = $("li a[data-value='" + def + "']", drop);
					if (li.find("h4").length > 0) toggle.html(li.find("h4").html()); else toggle.html(li.text());
				}
				if (arrow) $(".dropdown-toggle", drop).append(arrow);
			} else if (field.next().hasClass("btn-group") && field.next().find("button").length > 0) {
				field.next().removeClass("error").find("button").removeClass("active");
				if (def != null && typeof(def) != "undefined") field.next().find("button[data-value='" + def + "']").trigger("click");
			} else if (field.attr("type") == "checkbox") {
				field.closest(".checkradio").checkradio("clear");
			}
			if (def != null || typeof(def) != "undefined") field.val(def); else field.val("");
			
		},
		
		validar: function(field, condition) {
			if (condition) { 
			
				if (typeof(field) != "object") field = [field];
				//console.dir(field);
				for(i=0;i<field.length;i++) {
					if ($(field[i]).parent().hasClass("bestdrop")) {
						$(field[i]).parent().find("a.dropdown-toggle").addClass("error");
					} else if ($(field[i]).next().hasClass("btn-group")) {
						$(field[i]).next().addClass("error");
					} else if ($(field[i]).attr("type") == "checkbox") {
						//console.log("is check radio");
						$(field[i]).closest(".checkradio").addClass("error");
					} else if ($(field[i]).attr("type") == "radio") {
						$(field[i]).closest(".checkradio").addClass("error");
					} else {
						$(field[i]).addClass("error");
					}
				}
			}
			return !condition;
		},
		
		validarLogin: function($container) {
			var $sms = $(".password-sms", $container);
			var $neg = $(".password-negociacao", $container);
			var valid = true;
			var $this = this;
			
			// Validar Password de Negociação
			if ($(".digits input", $container).length > 0 && $neg.is(":visible")) {
				$(".digits input", $container).each(function() {
					valid = $this.validar($(this), $(this).val() == "") && valid;
				});
			}
			
			// Validar Código SMS
			if ($sms.length > 0 && $sms.is(":visible")) {
				$token = $("input[name='smscode']");
				valid = $this.validar($token, $token.val() == "" || $token.val().length < $token.attr("maxlength")) && valid;
			}
			
			// Há condicoes para aceitar?
			$condicoes = $(".bestalert input[type='checkbox']", $container);
			
			if ($condicoes.length > 0) {
				$condicoes.each(function() {
					valid = $this.validar($(this), $(this).attr("checked") != "checked") && valid;
				});	
			}
			
			return valid;
		},
		
		
		clearErrors: function() {
	        $("input, textarea, .btn-group, .dropdown-toggle, .checkradio label span, .btn-group", this.container).removeClass("error");
        },
		
		
		validarBulk: function($container) {
			var valid = true;
			var $this = this;
			
			$this.clearErrors();
			
			
			$("input, textarea", $container).each(function() {
				var i = $(this);
				var val = $.trim(i.val());
				var req = i.hasClass("required");
				var type = "TEXT";
				var readonly = i.attr("readonly") == "readonly";
				var disabled = i.attr("disabled") == "disabled";
				if (i.closest(".bestdrop").length > 0) type = "DROP";
				if (i.attr("type") == "checkbox") type = "CHECK";
				if (i.hasClass("date") || i.hasClass("time")) type = "DATETIME";
				var match = $(this).data("match");
				var numeric = i.data("min") || i.data("max") || i.data("restrict") == "INT" || i.data("restrict") == "DECIMAL";
				var empty = type == "CHECK" ? i.attr("checked") != "checked" : $.trim(i.val()) == "";
				var checkgroup = $(this).closest(".checkgroup") ;
				var password = i.attr("type") == "password";
				var minlength = i.data("minlength") || false;
				
				
				if (i.parent().is(":visible")) {
					if (checkgroup.length > 0) {
						req = checkgroup.hasClass("required");
						
						if (req) {
							var atleastone = false;
							var checkarray = [];
							$("input", checkgroup).each(function() {
								if ($(this).attr("checked") == "checked") atleastone = true;
								checkarray.push($(this));
							});
							//console.log("atleastone: " + atleastone);
							valid = $this.validar(checkarray, !atleastone) && valid;
						}
					} else if ((!readonly || password || type == "DATETIME") && !disabled) {
						if (minlength) {
							//console.dir(minlength && val.length < minlength && req && !empty);
						}
						var thisvalid = $this.validar(i, (req && empty) || (minlength && val.length < minlength && !empty) || (numeric && isNaN(val.replace(",", ".")) && !empty) || (!$this.validFormat(i) && !empty));
						
						if (match) {
							var matched = $("input[name='" + match + "']", $container);
							thisvalid = $this.validar([i, matched], val != matched.val()) && thisvalid;
							if (thisvalid) matched.removeClass("error");
						} 
						valid = thisvalid && valid;
					} else if (readonly) {
						//console.log(i.attr("name") + ": readonly");
					} else if (disabled) {
						//console.log(i.attr("name") + ": disabled");
					}
				}
			});
			return valid;
		},
		
		
		showError: function() {
			
			if ($(".error", this.container).length > 0) {
				if ($(".error:eq(0)").closest("password-necociacao").length > 0 || $(".error:eq(0)").closest("password-sms").length > 0) {
					$("html,body").animate({scrollTop: $(".password-negociacao", this.container).offset().top - 100}, {queue:false, duration: 500});
				} else {
					$("html,body").animate({scrollTop: $(".error:not(li.error):eq(0)", this.container).closest(".set_input, .set_row, .set_group, .container").eq(0).offset().top - 100}, {queue:false, duration: 500});	
				}
			} else {
				this.scrollToTop();
			}
				
		},
		
		showSuccess: function(tab) {
			tab.parent().find(".tab-pane").not(tab).removeClass("active");
			tab.addClass("active");
			$(".password-sms").hide();
			$(".password-negociacao").hide();
			$(".registered_alert").fadeIn(function() {
				setTimeout(function() {
					window.location.href="01_area_pessoal.html";
				}, 5000);
			});
			$("html,body").animate({scrollTop: this.container.offset().top - 100}, {queue:false, duration: 500});
		},
		
		showPasswordSMS: function() {
			$("html,body").animate({scrollTop: $(".password-sms", this.container).offset().top - 100}, {queue:false, duration: 500});	
		},
		
		scrollToTop: function() {
			$("html,body").animate({scrollTop: this.container.offset().top - 100}, {queue:false, duration: 500});
		},
		
		
		
		getValue: function(scope, name) {
			var input = $("input[name='" + name + "']", scope);
			if (input.length == 0) return {label: null, value: null};
			var ret = {};
			if (input.closest(".bestdrop").length > 0) {
				ret = {	
							label: input.closest(".bestdrop").find("a.dropdown-toggle").text(), 
							value: input.val() 
						};
			} else {
				ret = {	label: input.val(), 
							value: input.val()
						};
			}
			
			return ret;
		},
		
		setValue: function(scope, name, value) {
			var input = $("input[name='" + name + "']", scope);
			if (typeof(value) == "undefined" && input.attr("type") != "checkbox") return;
			
			
			this.resetField(input);
			this.clearErrors();
			if (input.closest(".bestdrop").length > 0) {
				input.closest(".bestdrop").find(".dropdown-menu a[data-value='" + value + "']").trigger("click");
				input.val(value);
			} else if (input.next().hasClass("btn-group") && input.next().find("button").length > 0) {
				var group = input.next();
				group.find("button").removeClass("active");
				group.find("button[data-value='" + value + "']").addClass("active");
				input.val(value);
			} else if (input.attr("type") == "checkbox") {
				input.attr("checked", false);
				input.closest("label").removeClass("checked");
				input.closest(".checkradio").checkradio("value", value);
			} else {
				input.val(value);
			}
			input.trigger("change");
		},
		
		
		resetAll: function(scope) {
			var input = $("input", scope);

			var $this = this;
			input.each(function() {
				if ($(this).closest(".set_input").is(":visible")) {
					//console.log("reset " + $(this).attr("name"));
					$this.resetField($(this));
				}
			});
		},
		
		timer: null,	
		
		cancelNotification: function() {
			clearTimeout(this.timer);
			$(".not_container").removeClass("sending");
			$(".not_container > div").hide();
			$(".not_container .not_main").show();
		},
		
		sendNotification: function(e, callbackok, callbackerror, callbackfinish) {
			
			this.cancelNotification();
			
			var $this = this;
			
			if (typeof(this.timer) != "undefined") clearTimeout($this.timer);
			
			var block = $(e.currentTarget).closest(".not_container");
			var main = $(".not_main", block);
			var sending = $(".not_sending", block);
			var error = $(".not_error", block);
			var success = $(".not_success", block);
			
			$("> div", block).hide();
			block.addClass("sending");
			$(".not_sending", block).show();
			$this.timer = setTimeout(function() {
				block.find(".not_sending", block).hide();
				block.find(".not_error", block).show();
				if (typeof(callbackerror) == "function") callbackerror.apply($this);
				$this.timer = setTimeout(function() {
					$(".not_error", block).hide();
					$(".not_success", block).show();
					if (typeof(callbackok) == "function") callbackok.apply($this);
				}, 2500);
				
			}, 2500);
			
			
			$(".close_not_error", block).unbind("click").click(function(){
				if (typeof($this.timer) != "undefined") clearTimeout($this.timer);
				$("> div", block).hide();
				main.show();
				block.removeClass("sending");
				callbackerror.apply(this);
			});
			
		    $(".close_not_success").unbind("click").click(function(){
		    	if (typeof($this.timer) != "undefined") clearTimeout($this.timer);
				callbackok.apply(this);
				$("> div", block).hide();
				block.removeClass("sending");
				$("a.close", block).trigger("click");
				if (typeof(callbackfinish) == "function") callbackfinish.apply($this);
			});
			
			
			
			
		}
		
		
		
		
			
	});
	
	$(document).ready(function() {		
		$("#menu_contascartoes").addClass("selected"); // SELECTS MENU HIGHLIGHT
		// - - - 
		ContasCartoes.All.init();		
		// - - -
		$(".pedido-contacto").click(function(){
			$(".simuladorbox .passo1").hide();
			$(".simuladorbox .passo2").show();
		});
		$(".simuladorbox .voltarazul").click(function(){
			$(".simuladorbox .passo2").hide();
			$(".simuladorbox .passo1").show();
		});

		
		$("[data-toggle=tooltip]").tooltip('destroy');
		$("[data-toggle=tooltip]").tooltip();	
	});

})(jQuery);