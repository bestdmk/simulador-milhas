var App = App || {};
App.Components = App.Components || {};

App.Components.RangeSlider = (function ($) {
    var o = {};
    var valueBubble = undefined;
    o.minStr = "";
    o.maxStr = "";

    o.initialize = function (options) {

        o.valueBubble = '<output class="rangeslider__value-bubble" />';

        $('input[type="range"]').rangeslider({ 
            polyfill: false,

            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',

            // Callback function
            onInit: function() {
              $rangeEl = this.$range;

              // $('.rangeslider__handle', $rangeEl).append('<div class="rangeslider__handle__value">' + this.value + '</div>');
              $($rangeEl).parent().append('<div class="rangeslider__handle__value"><input type="text"  name="rangerslider_value" value="'+this.value+'"></div>');
              $('.rangeslider__handle', $rangeEl).append('<div class="rangeslider__handle__value-handle">' + this.value + '</div>');
              
              // get range index labels 
              var rangeLabels = this.$element.attr('labels');
              rangeLabels = rangeLabels.split(', ');
              
              // add labels
              $rangeEl.append('<div class="rangeslider__labels"></div>');
              $(rangeLabels).each(function(index, value) {
                $rangeEl.find('.rangeslider__labels').append('<span class="rangeslider__labels__label">' + value + '</span>');
              })
            },

            // Callback function
            onSlide: function(position, value) {
              // $('.rangeslider__handle__value input', this.$range.parent()).val(value);

              var sufix = "";
              if(this.$element.attr("sufix")){
                sufix = " " + this.$element.attr("sufix");
              }

              var decimals = 0;
              if(this.$element.attr("decimals")){
                decimals = parseInt(this.$element.attr("decimals"));
              }
              // retirar decimals var
              
              var newValue = o.formattedNumber(parseFloat(value));
              newValue = o.replaceLast(newValue, ".", ",");

              $('.rangeslider__handle__value-handle', this.$range).text(newValue + sufix)


              App.Components.RangeSlider.updateLabels(this.$element, this.$range);
               
            }

            // Callback function
            // onSlideEnd: function(position, value) {}
        });

        // $(".rangeslider__handle__value").click(function(e){
        //   console.log("sdad");
        // })

        return o;
    };


    o.updateLabels = function (element, rangeEl) {
      
      var sufix = "";
      if(element.attr("sufix")){
        sufix = " " + element.attr("sufix");
      }

      var decimals = 0;
      if(element.attr("decimals")){
        decimals = parseInt(element.attr("decimals"));
      }

      o.minStr = element.attr('min');
      o.minStr = o.formattedNumber(parseFloat(element.attr('min')), decimals);
      o.minStr = o.replaceLast(o.minStr, ".", ",");

      o.maxStr = element.attr('max');
      o.maxStr = o.formattedNumber(parseFloat(element.attr('max')), decimals);
      o.maxStr = o.replaceLast(o.maxStr, ".", ",");


      rangeEl.find('.rangeslider__labels .rangeslider__labels__label:first-child').html(o.minStr  + sufix);
      rangeEl.find('.rangeslider__labels .rangeslider__labels__label:last-child').html(o.maxStr  + sufix);
    }


    o.updateValueBubble = function (pos, value, context) {
      pos = pos || context.position;
      value = value || context.value;
      var $valueBubble = $('.rangeslider__value-bubble', context.$range);
      var tempPosition = pos + context.grabPos;
      var position = (tempPosition <= context.handleDimension) ? context.handleDimension : (tempPosition >= context.maxHandlePos) ? context.maxHandlePos : tempPosition;

      if ($valueBubble.length) {
        $valueBubble[0].style.left = Math.ceil(position) + 'px';
        $valueBubble[0].innerHTML = value;
      }
    }

    

    
    o.resize = function() {

    }

    o.replaceLast = function(str, find, replace) {
        var nindex = str.split(find).length - 1;
        var index = str.lastIndexOf(find);

        if (index >= 0) {
            return str.substring(0, index) + replace + str.substring(index + find.length);
        }

        return str.toString();
    }

    o.formattedNumber = function(n,d, r){
      if (typeof(r) == "undefined") r = false;
      if (r) n = Math.round(n);
      if (d != 0) n = n.toFixed(d);
      
      var rx=  /(\d+)(\d{3})/;
      return String(n).replace(/^\d+/, function(w){
        while(rx.test(w)){
          w= w.replace(rx, '$1.$2');
        }
        return w;
      });
    }
    
    o.currencyFormatEuro= function  (num) {
        return num
           .toFixed(2) // always two decimal digits
           .replace(".", ",") // replace decimal point character with ,
           .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") // use . as a separator
    }
    
    o.formatDecimals= function  (num) {
        return num
           .toFixed(1) // always two decimal digits
           .replace(".", ",") // replace decimal point character with ,
           .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") // use . as a separator
    }
    
        // Correct format number with thousands for negatives
        // "True" Includes trailing decimals
    o.formatMoney = function(n,t){
      n = (Number(n).toFixed(2) + '').split('.');
      if (typeof(t) == "undefined") t = false;
      
      if(t==true){
        return n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ',' + (n[1] || '00');
      }else {
        return n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
      
    }

    return o;
    
}(jQuery));

// if (document.all) {
//     App.Reforma.Home.initialize();
// } else {
//     $(function () {
//         App.Reforma.Home.initialize();
//     });
// }