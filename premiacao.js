// ***************************************** 
// Description: Simulador Compras Especiais jQuery Plugin   
// *****************************************
(function($, window, document, undefined) {
	var pluginName = 'simuladorPremiacao',
		$this = null,
		self = this;
		
	var SimuladorPremiacao = function( element, options ) {
        this.element = $(element);
        this._name = pluginName + parseInt(Math.random() * 999999999);
			
		$this = this;
		
		this.elem = {
			
		};
		
        this.init(options);	
    }
	
	// Init plugin
	SimuladorPremiacao.prototype = {
        
		init: function(options) {
			
			
			this.options = options;
			this.elem.cartao = $("input[name='cartao']", this.element);
			this.elem.premiacao = $("input[name='premiacao']", this.element);			
			this.elem.limpar = $(".passo1 .btn-limpar", this.element);
			this.elem.calcular = $(".passo1 .btn-calcular", this.element);
			
			this.elem.quadro = $(".quadro", this.element);
			
			// this.elem.valorMilhas = $("#ceSliderValor_milhas", this.element);
			this.elem.valorCashback = $("#ceSliderValor_cashback", this.element);
			var valorValue = this.elem.valorCashback.next().next();
			this.elem.valorValue = $("input", valorValue);

			// console.log(this.elem.valorCashback)
			
			this.elem.grafico = $(".grafico", this.element);
			this.elem.display1 = $("#spDisplay1", this.element);
			this.elem.display2 = $("#spDisplay2", this.element);
			this.elem.display3 = $("#spDisplay3", this.element);
			this.elem.utilizacao = $("#spUtilizacao", this.element);
			this.elem.utilizacaoLinha = $("#spUtilizacaoLine", this.element);
			
			this.elem.lockValor = $(".parametro.sim-valor a.cadeado", this.element);

			this.elem.labely = $(".label-y", this.element);
			
			$("a.cadeado", this.element).tooltip();
			
			// this.limpar(null);

			this.elem.valorCashback.attr({
                    min: parseFloat(this.options.valorMin),
					max: parseFloat(this.options.valorMax),
					value: 300,
					step: 300,
                    labels: this.options.valorMin + "," + this.options.valorMax
                }
             );

			this.elem.valorCashback.rangeslider('update', true);
           	this.elem.valorCashback.on('change', this.sliderChanged);
			this.elem.valorValue.val(this.options.valor);
			
			// Allow only numbers
			this.elem.valorValue.bind("keydown", $.proxy(this.blockLetters, this));
			
			// Field updates
			this.elem.valorValue.bind("change", $.proxy(this.calculate, this));
			this.elem.valorValue.bind("blur", $.proxy(this.sliderValueUpdate, this));
			this.elem.cartao.bind("change", $.proxy(this.calculate, this));
			this.elem.premiacao.bind("change", $.proxy(this.calculate, this));
			// this.elem.valorMilhas.bind("change", $.proxy(this.calculate, this));
			// this.elem.valorCashback.bind("change", $.proxy(this.calculate, this));
			this.elem.valorValue.change($.proxy(this.calculate, this)).trigger("change");
			this.elem.valorValue.keyup($.proxy(this.calculate, this));
			
			this.elem.limpar.bind("click", $.proxy(this.limpar, this));
			this.elem.calcular.bind("click", $.proxy(this.validar, this));
			
			// Login Button
			// $("p.fazerlogin a").click(function() {	
			// 	try{
			// 		$(".content_login .button.button-login-top").trigger("click");
			// 	}catch(e){};
			// });
						
			
			// Clear button
			// $("input + a.best-input-icon").click(function() {
			// 	$(this).prev().val("0");
			// });
			

			var selected = undefined;
			$(".cartao-dropdown-menu").change(function(e) {
             
                selected = $(e.target.selectedOptions)[0];

                $this.elem.cartao.val($(selected).data("value"));
				$this.elem.cartao.data("points", $(selected).data("points"))
			    $this.elem.cartao.trigger("change");

            });


			this.calculate();
			
		},
		
		validar: function(e) {
			if (e) e.preventDefault();
			
		},
		
		
		blockLetters: function(event) {
			
			
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				 // Allow: Ctrl+A
				(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: home, end, left, right
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			else {
				// Ensure that it is a number and stop the keypress
				if (event.keyCode == "190" && $(event.currentTarget).val().length > 0) {
				
				} 
				else if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		},
		
		limpar: function(e) {
			if (e) e.preventDefault();
			ContasCartoes.All.setValue(this.element, "cartao", this.options.cartao);
			ContasCartoes.All.setValue(this.element, "premiacao", this.options.premiacao);
			ContasCartoes.All.setValue(this.element, "valor", this.options.valor);
			this.calculate();
		},
		
		sliderValueUpdate: function(e) {
			this.sliderValueChanged(e, true);
		},
		
		sliderValueChanged: function(e, checkmin) {
			if (typeof(checkmin) == "undefined") checkmin = false;
			var field = $(e.currentTarget);
			var s = $(e.currentTarget).parent().prev().prev();

			var minValue = s.attr("min");
			var maxValue = s.attr("max");		
			
			var val = parseFloat(field.val());
			
			if (checkmin) {
				if (val > parseFloat(maxValue)) field.val(maxValue);
				if (val < parseFloat(minValue)) field.val(minValue);
			}
			

			if (!isNaN(val)){

				s.attr({ value: field.val()});
	       		s.rangeslider('update', true);

	       		this.calculate.apply(this);
			}

		},


		
		sliderChanged:  function(e) {
			
			var target = e.currentTarget;
			var val = target.value;
			var valfield = $(target).parent().find(".rangeslider__handle__value input");

			if (parseInt(val) != 0 && val % parseInt(val) != 0 && val) val = val.toFixed(2);
			valfield.val(val);
			
			$this.calculate.apply($this);
		},
		
		
		switchLock: function(e) {
			$(e.currentTarget).toggleClass("locked");
			this.updateLocks();
		},
		
		checkLocked: function(e) {
			var isLocked = $(e.currentTarget).hasClass("locked");
			
			if ($(e.currentTarget).hasClass("locked")) {
				e.preventDefault();	
			}
			return isLocked;
		},
		
		updateLocks: function() {
			
			if (this.elem.lockValor.hasClass("locked"))  {
				this.elem.valorMilhas.addClass("locked");
				this.elem.valorCashback.addClass("locked");
				this.elem.valorMilhas.parent().parent().parent().addClass("locked");
				this.elem.valorCashback.parent().parent().parent().addClass("locked");
				this.elem.valorValue.attr("disabled", "disabled");
				this.elem.valorMilhas.slider("setLocked", true);
				this.elem.valorCashback.slider("setLocked", true);
			} else {
				this.elem.valorMilhas.removeClass("locked");
				this.elem.valorCashback.removeClass("locked");
				
				this.elem.valorMilhas.parent().parent().parent().removeClass("locked");
				this.elem.valorCashback.parent().parent().parent().removeClass("locked");
				
				this.elem.valorValue.removeAttr("disabled");
				
				this.elem.valorMilhas.slider("setLocked", false);
				this.elem.valorCashback.slider("setLocked", false);
			}
		},
		
		calculate: function() {			
			var maxY = 122;
			
			var val_retorno = 0;
			var val_retorno_1 = 0;
			var val_retorno_3 = 0;
			var tipo_adesao = this.elem.premiacao.val();
			var euro = "&euro;";
			
			var labely = this.elem.labely;
			
			
			

			if ( tipo_adesao == "milhas" ) { 
				// this.elem.quadro.removeClass('cashback').addClass('miles');
				
				// var valor = parseInt(this.elem.valorMilhas.val());
				// $(".alert-error").hide();
				// if(valor<200){
				// 	$(".jq_erro_milhas").show();
				// 	valor= 200;
				// 	$("#spValor").val(valor);				
				// }
				
				// //var val_adesao = 10000;
				// var val_adesao = this.elem.cartao.parent().find("ul li a[data-value='" + this.elem.cartao.val() + "']").data("miles");
				// var val_multiplicador = 0.5;
		
				// val_retorno = parseInt(valor) * val_multiplicador;
				// val_retorno_1 = val_retorno * 12;
				// val_retorno_3 = val_retorno * 36;
				
				// $("#ceSliderValor_milhas").parents(".slider").show();
				// $("#ceSliderValor_cashback").parents(".slider").hide();
			}else{ 

				//console.log("#### calculate cashback");


				// adesao cashback	
				var valor = parseInt(this.elem.valorCashback.val());
				$(".alert-error").hide();
				if(valor<300){
					$(".jq_erro_cashback").show();
					valor= 300;
					$("#spValor").val(valor);				
				}

				//console.log("calculate: ");
				//console.log(this.elem.cartao);
				
				//var val_adesao = 40;
				var val_adesao = this.elem.cartao.parent().find(".cartao-dropdown-menu option[data-value='" + this.elem.cartao.val() + "']").data("cashback");
				console.log(val_adesao)
				// console.log(val_adesao);
				// var val_adesao = this.elem.cartao.parent().find("ul li a[data-value='" + this.elem.cartao.val() + "']").data("cashback");
				var val_multiplicador = 0.0033333333333333;
				euro = " &euro;"
		
				val_retorno = Math.floor(valor * val_multiplicador * 100) / 100;
				val_retorno_1 = parseInt(valor * val_multiplicador *1200) / 100;
				val_retorno_3 = parseInt(valor * val_multiplicador *3600) / 100;
				
				// if($("#ceSliderValor_cashback").parents(".slider").is(":hidden")){
				// 	$("#ceInputValor").val("300");
				// }
				// $("#ceSliderValor_cashback").parents(".slider").show();
				// $("#ceSliderValor_milhas").parents(".slider").hide();
				
			}
			
			// valor adesao
			var val_ades_mes = 0;
			if (this.elem.cartao.val() != "0") {
				val_ades_mes = val_adesao;
			};	
				
			
			//NEW
			var mesString = "";
			var anoString = "";
			var anosString = "";

			var val1a = val_ades_mes.formatMoney(0, ',', '.') + euro;
			var val1b = anoString + "n/a";
			var val1c = anosString + "n/a";

			var val2a = mesString + val_retorno.formatMoney(0, ',', '.') + euro;
			var val2b = anoString + (val_retorno_1 + val_ades_mes).formatMoney(0, ',', '.') + euro;
			var val2c = anosString + (val_retorno_3 + val_ades_mes).formatMoney(0, ',', '.') + euro;

			var val3a = mesString + (val_ades_mes + val_retorno).formatMoney(0, ',', '.') + euro;
			var val3b = anoString + (val_retorno_1 + val_ades_mes).formatMoney(0, ',', '.') + euro;
			var val3c = anosString + (val_retorno_3 + val_ades_mes).formatMoney(0, ',', '.') + euro;


			// $('#val1a', this.elem.quadro).html(val_ades_mes.formatMoney(0, ',', '.') + euro);
			$('.val1a', this.elem.quadro).html(val1a);
			$('.val1b', this.elem.quadro).html(val1b);
			$('.val1c', this.elem.quadro).html(val1c);


			//console.log(this.elem);

			$('.val2a', this.elem.quadro).html(val2a);
			
			
			$('.val3a', this.elem.quadro).html(val3a);
		
			// $('#val2b, #val3b', this.elem.quadro).html( (val_retorno_1 + val_ades_mes).formatMoney(0, ',', '.') + euro);
			// $('#val2c, #val3c', this.elem.quadro).html( (val_retorno_3 + val_ades_mes).formatMoney(0, ',', '.') + euro);
			$('.val2b', this.elem.quadro).html(val2b);
			$('.val3b', this.elem.quadro).html(val3b);
			
			$('.val2c', this.elem.quadro).html(val2c);
			$('.val3c', this.elem.quadro).html(val3c);
			
			var max = val_retorno_3 + val_ades_mes;
			var divisorRetorno = max < 1000 ? 100 : (max < 10000 ? 1000 : (max < 100000 ? 5000 : 10000));
			var topRetorno = Math.ceil(max / divisorRetorno) * divisorRetorno;
			
			//if (val_ades_mes == 0) this.elem.utilizacao.hide(); else this.elem.utilizacao.show();
			//if (val_ades_mes == 0) this.elem.utilizacaoLinha.hide(); else this.elem.utilizacaoLinha.show();
			
			
			// this.elem.display1.height((val_retorno / topRetorno) * maxY).find("p").html($('#val2a', this.elem.quadro).html());
			// this.elem.display2.height(((val_retorno_1 + val_ades_mes) / topRetorno) * maxY).find("p").html($('#val2b', this.elem.quadro).html());
			// this.elem.display3.height(((val_retorno_3 + val_ades_mes) / topRetorno) * maxY).find("p").html($('#val2c', this.elem.quadro).html());
			// this.elem.utilizacao.height((val_retorno / topRetorno) * maxY).find("p").html($('#val1a', this.elem.quadro).html());
			
			
			// if (this.elem.display1.height() < 19) this.elem.display1.height(19);
			// if (this.elem.utilizacao.height() < 19) this.elem.utilizacao.height(19);
			
			
			// $($("li", labely).get().reverse()).each(function(index) {
			// 	var val = Math.round(topRetorno * (index / 5)).formatMoney(0, ',', '.');
			// 	$(this).text(val);
			// });
			
			
			
			
		}
		
    };
	
	$.fn[pluginName] = function ( option, val ) {
		return this.each(function () {
			var $this = $(this),
				data = $this.data(pluginName),
				options = typeof option === 'object' && option;
			if (!data)  {
				$this.data(pluginName, (data = new SimuladorPremiacao(this, $.extend({}, $.fn[pluginName].defaults, options))));
			}
			if (typeof option == 'string') {
				data[option](val);
			}
		})
	};

	
	$.fn[pluginName].defaults = {
		cartao: "gold_plus_visa",
		premiacao: "cashback",
		valor: 300,
		valorMin: 0,
		valorMax: 10000,
		valorStep: 300
	};

	$.fn[pluginName].Constructor = SimuladorPremiacao;
	
})(jQuery, window, document);