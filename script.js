
window.addEventListener('DOMContentLoaded', function() {
  var alterClass = function() {
    let resultsModalMiles = document.querySelector(".results-tapvitoria");
    let resultsModalCashback = document.querySelector(".results-premiacao");
    var widthWindow = document.body.clientWidth;
    if (widthWindow < 969) {
      resultsModalMiles.classList.add("modal", "fade");
      resultsModalCashback.classList.add("modal", "fade");
    } else if (widthWindow >= 969) {
      resultsModalMiles.classList.remove("modal", "fade");
      resultsModalCashback.classList.remove("modal", "fade");
    };
  };
  window.addEventListener('resize', function(event){
    alterClass();
  })
  alterClass();
})




/*Tabs Selector*/
function openTabs(evt, tabName, buttonLink) {
    var i, tabcontent, tabtext, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    tabtext = document.getElementsByClassName("tabcontenttext");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";	
    }
    for (i = 0; i < tabtext.length; i++){
        tabtext[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "flex";
    evt.currentTarget.className += " active";
    document.getElementById(buttonLink).style.display = "block";
}


/*Function show calculation miles */
let buttonToggle = document.querySelector(".btn-calc");
let iconToggleUp = document.querySelector(".fa-chevron-up");
let iconToggleDown = document.querySelector(".fa-chevron-down");
buttonToggle.addEventListener("click", function(){
    let next = this.nextElementSibling;
    if(next.style.display == "none") {
      next.style.display = "block";
      iconToggleUp.style.display = "block";
      iconToggleDown.style.display = "none";
      buttonToggle.childNodes[0].nodeValue= "Fechar cálculo";
    } else {
      next.style.display = "none";
      iconToggleUp.style.display = "none";
      iconToggleDown.style.display = "block";
      buttonToggle.childNodes[0].nodeValue= "Abrir cálculo";
      
    }
})

