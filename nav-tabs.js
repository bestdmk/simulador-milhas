var App = App || {};
App.Components = App.Components || {};
App.Components.NavTabs = (function ($) {
    var o = {};
    
    o.currentTabActive = undefined;
    o.currentTabActiveIndex = 0;
    o.tabActiveIndex = 0; // TAB 1 ACTIVE

    o.initialize = function (options) {

        console.log("NavTabs :: initialize ");
        
        //===============================================================
        // NAV TABS - GET TAB ACTIVE
         // o.navTabsContainer = $(".bb-nav-tabs-container");
         o.tabActiveIndex = 0; // TAB 1 ACTIVE
         o.currentTabActive = $(".bb-nav-tabs .nav-item.active"); // TAB 1 ACTIVE

         o.navtabs = $(".bb-nav-tabs");
         o.navtabs.each(function(index, e){
           e.currentTabActiveIndex = 0; 
           e.currentTabActive = $(".nav-item.active", e)[0];
           // o.selectTab(e, o.tabActiveIndex);

         })

         setTimeout(o.checkNavTabsHeight, 250);

         // console.log("o.navtabs");
         // console.log(o.navtabs);
         // o.selectTab(o.tabActiveIndex);
        
        
        // NAV TABS TAB CLICK
        $(".bb-nav-tabs .nav-link").click(function(){ 

          curentNavtabs = $(this).closest(".bb-nav-tabs")[0]
          tabActiveIndex = $(this).parent().data("tabindex");
          o.selectTab(curentNavtabs, tabActiveIndex);  
        })

        // NAV TAB - MOBILE COLLAPSED HEADER CLICK 
         var tabCollapseActiveIndex  = -1;
         $(".bb-jsNavTabTrigger").click(function(){ 
            setTimeout(o.checkNavTabsHeight, 350);
         })

        return o;
    };

    o.selectTab = function(curentNavtabs,  tabIndex) {
        
        console.log("CURRENT selectTab");
        console.log(curentNavtabs.currentTabActive);

        var contentID = undefined;

        if(curentNavtabs.currentTabActive != undefined){
          // console.log("curentNavtabs.currentTabActive");
          contentID = $("a", curentNavtabs.currentTabActive).attr("href");
          contentDom = $(contentID);


          $(curentNavtabs.currentTabActive).removeClass("tri-down");
          $(curentNavtabs.currentTabActive).removeClass("opened");
          $('.nav-link', curentNavtabs.currentTabActive).removeClass("active")
          // $('.nav-link', o.currentTabActive).removeClass("active")
         
          if(contentDom){
            if(contentDom.hasClass("active")){
                contentDom.removeClass("active").removeClass("show");
                // contentDom.addClass("active");
            }
          }
        }
        
        curentNavtabs.currentTabActive =   $('li.nav-item[data-tabindex="'+tabIndex+'"]', curentNavtabs); 
        console.log("curentNavtabs.currentTabActive");
        console.log($("a", curentNavtabs.currentTabActive))
        contentID = $("a", curentNavtabs.currentTabActive).attr("href");
        contentDom = $(contentID);






        if(!curentNavtabs.currentTabActive.hasClass("opened")){
          console.log("selectTab ####");
          curentNavtabs.currentTabActive.addClass("tri-down");
          curentNavtabs.currentTabActive.addClass("opened");
          if(contentDom){
              if(!contentDom.hasClass("active")){
                  contentDom.addClass("active").addClass("show");
                  // contentDom.addClass("active");
              }
            }
          } 

          setTimeout(o.checkNavTabsHeight, 350);
         
    }

    o.checkNavTabsHeight = function () { 

        $(".bb-nav-tabs-container").each(function(index, e){

          var h = $(".tab-content", $(this)).height() + 350;
          console.log("checkNavTabsHeight: ")
          // console.log(e)
          console.log("checkNavTabsHeight: " + h)
          $(this).css({"paddingBottom": h});

        })

        
    }

    o.resize = function() {

    }

    return o;
    
}(jQuery));

if (document.all) {
      App.Components.NavTabs.initialize();
} else {
    $(function () {
      App.Components.NavTabs.initialize();
    });
}