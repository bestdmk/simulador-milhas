/*
* Documentation file
*
* @copyright Copyright (C) 2017 Banco Best.
* @author Marcos Neves <marcoslneves@gmail.com> 
*/

var Env = Env || {};
var App = App || {};
App.Documentation = App.Documentation || {};

App.Documentation.HeroBoxes = (function ($) {
	var o = {};

	var bgHeroActive = undefined;
	var bgHeroActiveIndex = 0;

	var boxHeroActive = undefined;
	var boxHeroIndex = 0;
	var boxHeroOverIndex = 0;

	var rotateHeroTimer = 0;
	var rotateHeroCount = 1;
	var canRotateHero = true;
	var isRotateRunning = false;

	o.initialize = function (options) {
		//console.log("App.Documentation Hero-boxes :: initialize");
		$('.bb-boxes-hero__box').on('click',function(e){
	
			window.location.href=$(this).find('a').attr('href');
			
			})

		$('.bb-homepage__logged .bb-hero__card-bg').click(function(){
			
		var boxNumber = $('.bb-hero__card-bg[style*="opacity: 1"]').attr('data-box');
		
		$('.bb-homepage__logged .bb-boxes-hero__box[data-slick-index='+boxNumber+']').trigger("click");

		});
		// HERO BOXES
		// ======================================
		// SLIDER BOXES - ONLY VISIBLE ON MOBILE

		// 2 BOXES
		if ($('.--slick-2-boxes').length > 0) {

			if (!$('.--slick-2-boxes').hasClass('slick-initialized')) {

				$('.--slick-2-boxes').slick({
					dots: false,
					infinite: false,
					speed: 300,
					arrows: false,
					slidesToShow: 2,
					slidesToScroll: 2,
					responsive: [
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2,
								infinite: true,
								dots: false
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2
							}
						},
						{
							breakpoint: 600,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								dots: true
							}
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								dots: true
							}
						}
					]
				});

			}
		}

		//console.log($('.--slick-3-boxes'))

		// 3 BOXES
		if ($('.--slick-3-boxes').length > 0) {

			$('.--slick-3-boxes').each(function () {

				if (!$(this).hasClass('slick-initialized')) {

					$(this).slick({
						dots: false,
						infinite: false,
						speed: 300,
						arrows: false,
						slidesToShow: 3,
						slidesToScroll: 3,
						responsive: [
							{
								breakpoint: 1024,
								settings: {
									slidesToShow: 3,
									slidesToScroll: 3,
									infinite: true,
									dots: false
								}
							},
							{
								breakpoint: 820,
								settings: {
									slidesToShow: 2,
									slidesToScroll: 2,
									centerMode: true,
									initialSlide: 1,
									centerPadding: '60px',
									variableWidth: false,
									dots: true
								}
							},
							{
								breakpoint: 576,
								settings: {
									slidesToShow: 1,
									slidesToScroll: 1,
									centerMode: true,
									initialSlide: 1,
									centerPadding: '30px',
									variableWidth: false,
									dots: true
								}
							},
							{
								breakpoint: 330,
								settings: {
									slidesToShow: 1,
									slidesToScroll: 1,
									centerMode: true,
									initialSlide: 1,
									centerPadding: '10px',
									variableWidth: false,
									dots: true
								}
							}
						]
					});

				}
			})
		}




		// ONLY ACTIVE IF HAS bb-hero__card-bg ( HERO COM BG IMAGE A ALTERAR NO HOVER DO BOX )
		if ($(".bb-hero__card-bg").length > 0 && !App.isMobile) {
			o.startRotateHeroBox();
			// ======================================
			// HERO - BOX HERO OVER TO CHANGE BG HERO
			$(".bb-boxes-hero__box__body").each(function (index, e) {
				$(e).mouseover(function () {

					if (o.isRotateRunning) {
						o.canRotateHero = false;
						clearInterval(o.rotateHeroTimer);
						/*  Don´t delete
						o.hideHeroBG();
						*/
						$(o.boxHeroActive).attr("style", "transform: perspective( 600px ) translateZ(0px)");
					}

					o.boxHeroOverIndex = $(this).data("boxindex");
					o.showHeroBG();
				})
				$(e).click(function(){
					window.location.href($(this).find('a').attr('href'));
				})


			
				
				/* Don´t delete
				$(e).mouseout(function () {
					o.hideHeroBG();
				})
				*/
			});
		}


		return o;

	}

	o.rotateHeroBox = function () {

		if (canRotateHero) {

			o.isRotateRunning = true;

			if (o.rotateHeroCount <= 4) {

				var box = $('.bb-box-' + o.rotateHeroCount)[0];

				console.log("rotateHeroCount:" + o.rotateHeroCount);

				switch (o.rotateHeroCount) {
					case 1:
						$(box).attr("style", "transform: perspective( 600px ) translateZ(20px) translateX(0px) !important");
						break;

					case 2:
						$(box).attr("style", "transform: perspective( 600px ) translateZ(20px) translateX(0px) !important");
						break;

					case 3:
						$(box).attr("style", "transform: perspective( 600px ) translateZ(20px) translateX(0px) !important");
						break;
				}

				$(o.boxHeroActive).attr("style", "transform: perspective( 600px ) translateZ(0px)");
				o.boxHeroActive = box;
				if(o.rotateHeroCount==4){
					o.boxHeroOverIndex=2;
				}else{
					o.boxHeroOverIndex = o.rotateHeroCount;

				}
				o.showHeroBG();
				o.rotateHeroCount++;


				// CHANGE setInterval Time 
				if (o.rotateHeroCount > 1) {
					clearInterval(o.rotateHeroTimer);
					o.rotateHeroTimer = setInterval(o.rotateHeroBox, 3000);
				}

			} else {
				clearInterval(o.rotateHeroTimer);
				o.rotateHeroCount = 1;
			
				
			}

		} else {
			
			o.isRotateRunning = false;
			clearInterval(o.rotateHeroTimer);
		
			
		}

	}

	o.startRotateHeroBox = function () {
		o.canRotateHero = true;
		o.rotateHeroCount = 1;
		o.rotateHeroTimer = setInterval(o.rotateHeroBox, 500);
	}

	o.resize = function () {

	}

	o.showHeroBG = function () {
		if (bgHeroActive != undefined) {
			bgHeroActive.css("opacity", 0);
		}

		bgHeroActive = $(".bb-hero__card-bg-" + o.boxHeroOverIndex);

		if (bgHeroActive != undefined) {
			bgHeroActive.css("opacity", 1);
		}
	}
	/* Dont´delete
	o.hideHeroBG = function () {
		if (bgHeroActive != undefined) {
			bgHeroActive.css("opacity", 0);
			bgHeroActive = undefined;
		}

	}
	*/
	return o;

}(jQuery));



/************************/
/* LOAD                         */
/************************/

if (document.all) {
	App.Documentation.HeroBoxes.initialize();
} else {
	$(function () {
		App.Documentation.HeroBoxes.initialize();
	});
}
