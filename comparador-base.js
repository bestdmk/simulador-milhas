var App = App || {};
App.Comparadores = App.Comparadores || {};

App.Comparadores.Base = (function ($) {
  var o = {};

  o.initialize = function (options) {

    console.log("App.Comparadores.Base :: init");


    /*// ======== // SET HEIGHT OF SUBTITLE AND ITEMS TO BE EQUALS - LIKE A TABLE
    o.nItems = $(".bb-comparador__item").length;
    var nSubitems = $(".bb-comparador__item__subitem").length / o.nItems;

    var hTitle;
    var dTitle;
    var currentTitleHeight;
    var maxTitleHeight = 0;

    var hSubtitle;
    var dSubtitle;
    var currentSubtitleHeight;
    var maxSubtitleHeight = 0;

    var hItem;
    var dItens = undefined;
    var dProduct;

    var maxItemHeight = 0;
    var currentItemHeight;
    var arrAllItens = [];
    var currentSubItemHeight;
    var currentSubItem;
    o.products = [];

    // ======== // TITLE AND SUBTITLE
    for (var i = 1; i <= o.nItems; i++) {
      dProduct = $(".bb-comparador__product-" + i)[0];

      // console.log(dProduct);
      o.products.push(dProduct);
      // TITLE
      dTitle = $("h5.bb-comparador__item--title", dProduct)[0];
      currentTitleHeight = $(dTitle).height();

      // console.log(dProduct);

      if (currentTitleHeight > maxTitleHeight) {
        maxTitleHeight = currentTitleHeight;
      }

      //SUBTITLE
      dSubtitle = $("p.bb-comparador__item--subtitle", dProduct)[0];
      currentSubtitleHeight = $(dSubtitle).height();

      if (currentSubtitleHeight > maxSubtitleHeight) {
        maxSubtitleHeight = currentSubtitleHeight;
      }

      console.log("maxSubtitleHeight: " + maxSubtitleHeight);
      console.log("==================");
    }


    for (var i = 1; i <= o.nItems; i++) {
      // dProduct = $(".bb-comparador__product-" + i)[0];
      dProduct = o.products[i];

      dTitle = $("h5.bb-comparador__item--title", dProduct)[0];
      $(dTitle).height(maxTitleHeight + 20);

      dSubtitle = $("p.bb-comparador__item--subtitle", dProduct)[0];
      $(dSubtitle).height(maxSubtitleHeight + 30);
    }

    // console.log('###########');
    // console.log($('a[data-colindex="1"][data-rowindex="1"]'));

    // ======== // ITEMS  - GRAY UL
    for (var j = 1; j <= nSubitems; j++) {
      maxItemHeight = 0;
      currentItemHeight = 0;
      for (var i = 1; i <= o.nItems; i++) {
        // currentSubItem = $('a[data-col="#product-'+i+'__collapse-' + j + '"]');
        currentSubItem = $($('a[data-colindex="' + i + '"][data-rowindex="' + j + '"]'));
        currentItemHeight = Math.ceil($(currentSubItem[0]).text().length / 38) * 17;

        console.log("ITEM :" + i + " - SUB: " + j);
        console.log(Math.ceil($(currentSubItem[0]).text().length / 16));
        if ($(currentSubItem[0]).text().length == 1) {
          // console.log("yo");
          // emptySubItems++;
          // console.log(emptySubItems);
          // console.log(currentSubItem);
          // __empty
          $($('a[data-colindex="' + i + '"][data-rowindex="' + j + '"]').parent()).parent().addClass("__empty");


        }
        // console.log($(currentSubItem[0]).css("height"));
        console.log(currentSubItem);
        console.log("#############");

        if (currentItemHeight > maxItemHeight) {
          maxItemHeight = currentItemHeight;
        }
      }

      // console.log("maxItemHeight :" + );
      // console.log(maxItemHeight);

      for (var i = 1; i <= o.nItems; i++) {
        currentSubItem = $($('a[data-colindex="' + i + '"][data-rowindex="' + j + '"]'));
        $(currentSubItem[0]).height(maxItemHeight);

        // console.log("currentSubItem");
        // console.log(currentSubItem.closest(""));
      }

    }*/

    //==========================------==========================
    //==========================--CD--==========================
    //==========================------==========================

    //----- CALCULAR E APLICAR ALTURA A ITENS DOS CARTS -----
    const calculateItemsHeight = () => {

      let numOfCards = $('.bb-comparador__item').length;
      let numOfItems = $('.bb-comparador__item:first-child .bb-comparador__item-body > div > *').length;

      let titlesHeight = [];
      let itemsHeight = [];

      //---------- GET HEIGHT FROM TITLES AND SUBTITLES ----------
      for(let item = 1; item <= 2; item++){
        titlesHeight.push([]);
        for(let card = 1; card <= numOfCards; card++){
          titlesHeight[item-1].push($('.bb-comparador__item:nth-child('+card+') .bb-comparador__item-body > div > *:nth-child('+item+')').outerHeight());
        }
      }

      for(let i = 1; i <= 2; i++){
          $('.bb-comparador__item .bb-comparador__item-body > div > *:nth-child('+i+')').outerHeight(Math.max(...titlesHeight[i-1]));
      }

      //---------- GET HEIGHT FROM ITEMS ----------
      for(let item = 3; item <= numOfItems; item++){
        itemsHeight.push([]);
        for(let card = 1; card <= numOfCards; card++){
          itemsHeight[item-3].push($('.bb-comparador__item:nth-child('+card+') .bb-comparador__item-body > div > *:nth-child('+item+') .bb-comparador__item__subitem-title a').outerHeight());
        }
      }

      for(let s = 3; s <= numOfItems; s++){
          $('.bb-comparador__item .bb-comparador__item-body > div > *:nth-child('+s+') .bb-comparador__item__subitem-title a').outerHeight(Math.max(...itemsHeight[s-3]));
      }
    }

    $(function(){
      calculateItemsHeight();
      $(window).resize(calculateItemsHeight());
    })

    //----- IDENTIFICAR TODOS OS ITENS DO MESMO NIVEL QUANDO HÁ MOUSE OVER -----
    $('.bb-comparador__item__subitem a.btn').mouseenter(function(){
      let itemIndice = $(this).closest('.bb-comparador__item__subitem').index()+1;
      let mi = $('.bb-comparador__item__subitem:nth-child('+itemIndice+') .bb-comparador__item__subitem-expand-content').text();

      $('.bb-comparador__item-body :nth-child('+itemIndice+') a.btn').css('font-weight','500');

      if(mi != ""){
        $('.bb-comparador__item__subitem:nth-child('+itemIndice+') a.btn').css('cursor','pointer');
      }
    });
    
    $('.bb-comparador__item__subitem a.btn').mouseleave(function(){
      let itemIndice = $(this).closest('.bb-comparador__item__subitem').index()+1;
      $('.bb-comparador__item-body :nth-child('+itemIndice+') a.btn').css('font-weight','400');
    });

    //==========================------==========================
    //==========================--CD--==========================
    //==========================------==========================

    $('.bb-jsCollapseTrigger').click(function (e) {
      var colindex = $(this).data("colindex");
      var rowindex = $(this).data("rowindex");

      // var itemIndex = $(this).data("target").split("__")[0];
      // itemIndex = itemIndex.substr(-1);

      //console.log("itemIndex");
      //console.log(colindex);
      //console.log(rowindex);

      //----- TOGGLE DOW ARROW ICONE -----
      let thisIndice = $(this).closest('.bb-comparador__item__subitem').index()+1;
      if($('.bb-comparador__item__subitem:nth-child('+thisIndice+') .bb-comparador__item__subitem-expand-content').hasClass('--opened')){
        $('.bb-comparador__item__subitem:nth-child('+thisIndice+') .btn span').html('&#11206;');
      }else{
        $('.bb-comparador__item__subitem:nth-child('+thisIndice+') .btn span').html('&#11205;');        
      }

      o.toogleCollapseSubitems(colindex, rowindex);
      // e.preventDefault();

      // bb-comparador__item-1__subitem-2

    })

    $('a[data-toggle="collapse"]').click(function (e) {
      // console.log("collapse");
      /*var subitemIndex = $(this).data("target").substr(-1);
      //console.log($(this));

      var itemIndex = $(this).data("target").split("__")[0];
      itemIndex = itemIndex.substr(-1);
      o.toogleCollapseSubitems(itemIndex, subitemIndex);
      e.preventDefault();*/

      //console.log("itemIndex");
      //console.log(itemIndex);
    })

    $(".tab-content-cartoes").click(function(e){
        $("html, body").animate({
            scrollTop: 2000
        }, 100);
    })

    // $("btn-link").hover(function(e){
    // })

    $(".bb-comparador__product-2").hover(function (e) {
      $(".bb-comparador__product-1 .bb-comparador__item-body").removeClass("col-active");
    })

    $(".bb-comparador__product-3").hover(function (e) {
      $(".bb-comparador__product-1 .bb-comparador__item-body").removeClass("col-active");
    })

    $(".bb-comparador__product-4").hover(function (e) {
      $(".bb-comparador__product-1 .bb-comparador__item-body").removeClass("col-active");
    })


    // INIT SLICK

    // ====== COMPARADOR 2 ITEMS
    if ($('.bb-slick__comparador-2items').length > 0) {
      if (!$('.bb-slick__comparador-2items').hasClass('slick-initialized')) {
        $('.bb-slick__comparador-2items').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 2,
          slidesToScroll: 2,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
              }
            }
          ]
        });

      }
    }

    // ====== COMPARADOR 3 ITEMS
    console.log($('.bb-slick__comparador-3items').length);
    if ($('.bb-slick__comparador-3items').length > 0) {

      if (!$('.bb-slick__comparador-3items').hasClass('slick-initialized')) {
        $('.bb-slick__comparador-3items').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 3,
          slidesToScroll: 3,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
              }
            }
          ]
        });

      }
    }

    // ====== COMPARADOR 4 ITEMS
    if ($('.bb-slick__comparador-4items').length > 0) {
      if (!$('.bb-slick__comparador-4items').hasClass('slick-initialized')) {

        $('.bb-slick__comparador-4items').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
              }
            }
          ]
        });

      }
    }
    return o;
  };

  o.toogleCollapseSubitems = function (colindexClicked, rowindexClicked) {

    $(".bb-comparador__item__subitem-expand-content").each(function (index, el) {

      var colindex = $(this).data("colindex");
      var rowindex = $(this).data("rowindex");

      // console.log(colindex);
      // console.log(rowindex);
      // console.log("############");

      if (rowindex == rowindexClicked) {

        // console.log("#333333333. 333 3############");
        $(this).toggleClass("--opened");
      } else {
        $(this).removeClass("--opened");
      }
      // if($(this).)
    });
  }

  o.resize = function () {

  }

  return o;

}(jQuery));


if (document.all) {

  App.Comparadores.Base.initialize();
} else {
  $(function () {
    App.Comparadores.Base.initialize();
  });
}