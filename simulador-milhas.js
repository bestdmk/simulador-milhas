var App = App || {};
App.Simuladores = App.Simuladores || {};

App.Simuladores.Milhas = (function($) {
    var o = {};
    var currentSlideActive = undefined;
    var currentSlideIndex = 0;
    var boxesAlreadyOpened = false;

    var currentTabActive = undefined;
    var currentTabActiveIndex = 0;

    o.initialize = function(options) {


        $(".rangeslider").css("width", "100%");
        $("#tabSimuladorTAPVitoria").simuladorTAPVitoria({});
        $(".rangeslider__handle__value", $("#ceSliderValor").parent()).css("opacity", 0);
        

        

        // const hastooltip = $(".hastooltip")
        // console.log(hastooltip.length);

        // const hastooltip = document.querySelectorAll('.hastooltip')
        // tippy(hastooltip, {
        //   theme: "bb-best-tooltip",
        //   position: 'top',
        //   animation: 'scale',
        //   duration: 250,
        //   arrow: true,
        //   distance: 10
        // });


        
        // hastooltip._tippy.show()
        

        return o;
    };


    o.resize = function() {

    }

    return o;

}(jQuery));

if (document.all) {
    App.Simuladores.Milhas.initialize();
} else {
    $(function() {
        App.Simuladores.Milhas.initialize();
    });
}